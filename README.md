# **KMDb**

O KMDb é uma plataforma simplificada de cadastro e reviews de filmes.

### **Instalando e rodando o projeto**

Baixando o projeto:

```sh
git clone https://gitlab.com/acuglar/kmdb.git
```

Depois de baixar, é necessário entrar na pasta, criar um ambiente virtual e ativa-lo:

```sh
cd kanvas  # Entrar na pasta onde o projeto foi baixado
python3 -m venv venv  # Criar um ambiente virtual
source venv/bin/activate  # Ativar ambiente virtual
```

Instalando dependências:

```sh
pip install -r requirements.txt
```

Rodando as migrations para que o banco de dados e as tabelas sejam criadas:

```sh
./manage.py migrate
```

Rodando o projeto kanvas localmente:

```sh
./manage.py runserver
# O sistema estará rodando em http://127.0.0.1:8000/
```

Rodando os testes:
```sh
python manage.py test -v 2 &> report.txt
coverage report > coverage_report.txt  # relatório de cobertuda dos testes do coverge
```
### **Utilização**

Para utilizar este sistema, é necessário utilizar um API Client, como o [Insomnia](https://insomnia.rest/download)

Esta plataforma tem 2 tipos de usuário:

- Crítico - possui o campo `is_staff` == True e `is_superuser` == False
- Admin - possui os campos `is_staff` e `is_superuser` com o valor True

## **Rotas**

`POST /api/accounts/` - Cria usuário

`POST /api/login/` - Autentica usuários

`POST /api/movies/` - Cria filme (apenas admin)

`GET /api/movies/ `- Lista todos os filmes (possível filtrar por título do filme)

`GET /api/movies/<int:movie_id>/`- Retorna filme pelo id, incluindo as reviews

`DELETE /api/movies/<int:movie_id>/`- Deleta filme (apenas admin)

`POST /api/movies/<int:movie_id>/review/` - Cria review, com o usuário logado (apenas crítico)

`GET /api/reviews/` - Lista as reviews, mudando o resultado se for admin ou crítico

`PUT /api/movies/<int:movie_id>/review/` - Atualiza a crítica do usuário (apenas crítico)

### **Criação de usuários:**

**POST /api/accounts/**

```json
// REQUEST
{
  "username": "critic",
  "password": "1234",
  "first_name": "John",
  "last_name": "Wick",
  "is_superuser": false,
  "is_staff": true
}

// RESPONSE STATUS -> HTTP 201
{
  "id": 1,
  "username": "critic",
  "first_name": "John",
  "last_name": "Wick",
  "is_superuser": false,
  "is_staff": true
}
// Caso haja a tentativa de criação de um usuário que já está cadastrado o sistema deve responder:
// RESPONSE STATUS -> HTTP 400 (Bad Request).
{
  "username": [
      "A user with that username already exists."
  ]
}
```


### **Autenticação:**

A API funcionará com autenticação baseada em token.

Esse token servirá para identificar o usuário em cada request.  
Na grande maioria dos endpoints seguintes, será necessário colocar essa informação nos Headers na API CLient no formato: Authorization: Token `<token gerado pela rota login>`.

**POST /api/login/**

```json
// REQUEST
{
  "username": "critic",
  "password": "1234"
}

// RESPONSE STATUS -> HTTP 200 (Ok)
{
  "token": "dfd384673e9127213de6116ca33257ce4aa203cf"
}

// Caso haja a tentativa de login de uma conta que ainda não tenha sido criada:
// RESPONSE STATUS -> HTTP 401 (Unauthorized).
```

### **Permissões:**

Todos os endpoints que exigirem a utilização de um token de acesso deverão responder da seguinte maneira:

- Token inválido

```json
// REQUEST
// Header -> Authorization: Token <token-inválido>

// RESPONSE STATUS -> HTTP 401 (Unauthorized)
{
  "detail": "Invalid token."
}
```

- Token válido, porém não atenda aos requisitos mínimos de permissão do tipo de usuário

```json
// REQUEST
// Header -> Authorization: Token <token-não-atende-aos-requisitos>

// RESPONSE STATUS -> HTTP 403 (Forbidden)
{
  "detail": "You do not have permission to perform this action."
}
```

---

### **Movies**:

**POST /api/movies/**  
Somente admin, cria filmes na plataforma KMDb

```json
// REQUEST
// Header -> Authorization: Token <token-do-admin>
{
  "title": "O Poderoso Chefão 2",
  "duration": "175m",
  "genres": [
    { "name": "Crime" }, 
    { "name": "Drama" }
    ],
  "premiere": "1972-09-10",
  "classification": 14,
  "synopsis": "Don Vito Corleone (Marlon Brando) é o chefe de uma 'família' ..."
}

// RESPONSE STATUS -> HTTP 201 (Created)
{
  "id": 1,
  "title": "O Poderoso Chefão 2",
  "duration": "175m",
  "genres": [
    {
      "id": 1,
      "name": "Crime"
    },
    {
      "id": 2,
      "name": "Drama"
    }
  ],
  "premiere": "1972-09-10",
  "classification": 14,
  "synopsis": "Don Vito Corleone (Marlon Brando) é o chefe de uma ..."
}
// Não deve ser possível criar dois gêneros com o mesmo nome, caso isso aconteça, a aplicação deverá retornar o gênero que está cadastrado no sistema.
```

**GET /api/movies/**  
Lista todos os filmes cadastrados

Este endpoint pode ser acessado por qualquer client (mesmo sem autenticação).

Nessa listagem não deve ser retornando o campo com as reviews do filme.

```json
// RESPONSE STATUS -> 200 OK
[
  {
    "id": 1,
    "title": "O Poderoso Chefão 2",
    "duration": "175m",
    "genres": [
      {
        "id": 1,
        "name": "Crime"
      },
      {
        "id": 2,
        "name": "Drama"
      }
    ],
    "premiere": "1972-09-10",
    "classification": 14,
    "synopsis": "Don Vito Corleone (Marlon Brando) é o chefe de uma 'família' ..."
  },
  {
    "id": 2,
    "title": "Um Sonho de Liberdade",
    "duration": "142m",
    "genres": [
      {
        "id": 2,
        "name": "Drama"
      },
      {
        "id": 4,
        "name": "Ficção científica"
      }
    ],
    "premiere": "1994-10-14",
    "classification": 16,
    "synopsis": "Andy Dufresne é condenado a duas prisões perpétuas..."
  }
]
```

**GET /api/movies/**  

Rota que lista todos os filmes cadastrados com base no filtro do request

Este endpoint pode ser acessado por qualquer client (mesmo sem autenticação).

Nessa listagem não deve ser retornando o campo com as reviews do filme.

```json
// REQUEST
{
  "title": "liberdade" // Campo obrigatório
}

// RESPONSE STATUS 200 (Ok)
[
  {
    "id": 2,
    "title": "Um Sonho de Liberdade",
    "duration": "142m",
    "genres": [
      {
        "id": 2,
        "name": "Drama"
      },
      {
        "id": 3,
        "name": "Ficção científica"
      }
    ],
    "premiere": "1994-10-14",
    "classification": 16,
    "synopsis": "Andy Dufresne é condenado a duas prisões perpétuas..."
  },
  {
    "id": 3,
    "title": "Em busca da liberdade",
    "duration": "175m",
    "genres": [
      {
        "id": 2,
        "name": "Drama"
      },
      {
        "id": 4,
        "name": "Obra de época"
      }
    ],
    "premiere": "2018-02-22",
    "classification": 14,
    "synopsis": "Representando a Grã-Bretanha,  corredor Eric Liddell"
  }
]
```

**GET /api/movies/<int:movie_id>/**  
Rota que busca o filme especificado pelo id

Este endpoint pode ser acessado por qualquer client (mesmo sem autenticação).  

Caso o usuário esteja autenticado, um campo com as reviews será mostrado juntamente com o retorno, caso contrário não será mostrado o campo com as reviews.

```json
// REQUEST
// Header -> Authorization: Token <token-do-critic ou token-do-admin>

// RESPONSE STATUS -> HTTP 200 OK
{
  "id": 9,
  "title": "Nomadland",
  "duration": "110m",
  "genres": [
    {
      "id": 2,
      "name": "Drama"
    },
    {
      "id": 4,
      "name": "Obra de Época"
    }
  ],
  "premiere": "2021-04-15",
  "classification": 14,
  "synopsis": "Uma mulher na casa dos 60 anos que, depois de perder...",
  "reviews": [
    {
      "id": 5,
      "critic": {
        "id": 1,
        "first_name": "Jacques",
        "last_name": "Aumont"
      },
      "stars": 8,
      "review": "Nomadland apresenta fortes credenciais para ser favorito ...",
      "spoilers": false
    }
  ]
}
```

Caso um usuário anônimo tente acessar esse endpoint:

```json
// RESPONSE STATUS -> HTTP 200 OK
{
  "id": 9,
  "title": "Nomadland",
  "duration": "110m",
  "genres": [
    {
      "id": 2,
      "name": "Drama"
    },
    {
      "id": 4,
      "name": "Obra de Época"
    }
  ],
  "premiere": "2021-04-15",
  "classification": 14,
  "synopsis": "Uma mulher na casa dos 60 anos que, depois de perder..."
}

//Caso seja passado um movie_id inválido:
// RESPONSE STATUS -> HTTP 404 (Not Found)
{
  "detail": "Not found."
}
```

**DELETE /api/movies/<int:movie_id>/**  
Somente admin, rota que deleta filmes

Ao excluir um filme da plataforma, também devem ser removidos todos os reviews.

```json
// REQUEST
// Header -> Authorization: Token <token-do-admin>

// RESPONSE STATUS -> HTTP 204 (No Content)

//Caso seja passado um movie_id inválido:
// RESPONSE STATUS -> HTTP 404 NOT FOUND (Not Found)
{
  "detail": "Not found."
}
```

### **Reviews:**

Esse endpoint só poderá ser acesso por um usuário do tipo crítico, e será responsável por criar reviews baseado nos filmes na plataforma KMDb.

**POST /api/movies/<int:movie_id>/review/** - Rota de criação de um review de um crítico

Agora que temos filmes cadastrados na plataforma, os críticos poderão realizar avaliações para eles.

**Importante!**

O campo stars aceita somente valores de 1 a 10.

```json
// REQUEST
// Header -> Authorization: Token <token-de-critic>
{
  "stars": 7,
  "review": "O Poderoso Chefão 2 podia ter dado muito errado...",
  "spoilers": false
}

// RESPONSE STATUS 201 CREATED
{
  "id": 1,
  "critic": {
    "id": 1,
    "first_name": "Jacques",
    "last_name": "Aumont"
  },
  "stars": 7,
  "review": "O Poderoso Chefão 2 podia ter dado muito errado...",
  "spoilers": false
}
```

Caso o usuário crítico já tiver feito uma revisão para o filme especificado, deve retornar o status `HTTP - 422 UNPROCESSABLE ENTITY`.

```json
// RESPONSE STATUS -> HTTP 422 UNPROCESSABLE ENTITY
{
  "detail": "You already made this review."
}
```

Caso seja passado um `movie_id` inválido, deverá retornar um erro `HTTP 404 - NOT FOUND`.

```json
// RESPONSE STATUS -> HTTP 404 NOT FOUND
{
  "detail": "Not found."
}
```

Caso seja passado um valor para "stars" fora da faixa de 1 a 10, o sistema deverá ter o seguinte retorno:

```json
// RESPONSE STATUS -> HTTP 400 BAD REQUEST
// Caso seja passado um valor acima de 10
{
    "stars": [
        "Ensure this value is less than or equal to 10."
    ]

// Caso seja passado um valor abaixo de 1
{
    "stars": [
        "Ensure this value is greater than or equal to 1."
    ]
}
```

**GET /api/reviews/**  
Acesso por critic ou admin. Rota que lista as reviews que foram realizadas

Caso o usuário seja admin deve ser listada todas as reviews:

```json
// REQUEST
// Header -> Authorization: Token <token-do-admin>

// RESPONSE STATUS -> HTTP 200 (Ok)
[
   {
      "id":1,
      "critic":{
         "id":1,
         "first_name":"Jacques",
         "last_name":"Aumont"
      },
      "stars":2,
      "review":"O Poderoso Chefão 2 podia ter dado muito certo..",
      "spoilers":true,
      "movie": 1
   },
   {
      "id":2,
      "critic":{
         "id":2,
         "first_name":"Bruce",
         "last_name":"Wayne"
      },
      "stars": 8,
      "review":"Não consegui ver até o final, fiquei com medo",
      "spoilers":false,
      "movie": 2
   },
   {
      "id":3,
      "critic":{
         "id":2,
         "first_name":"Bruce",
         "last_name":"Wayne"
      },
      "stars":10,
      "review":"Melhor filme que já assisti",
      "spoilers":true
      "movie": 1
   }
]

```
Caso seja um crítico deve listar apenas as críticas do próprio usuário:

```json
// REQUEST
// Header -> Authorization: Token <token-do-critic>

// RESPONSE STATUS -> HTTP 200 (Ok)
[
  {
      "id":2,
      "critic":{
         "id":2,
         "first_name":"Bruce",
         "last_name":"Wayne"
      },
      "stars": 8,
      "review":"Não consegui ver até o final, fiquei com medo",
      "spoilers":false,
      "movie": 2
   },
   {
      "id":3,
      "critic":{
         "id":2,
         "first_name":"Bruce",
         "last_name":"Wayne"
      },
      "stars":10,
      "review":"Melhor filme que já assisti",
      "spoilers":true,
      "movie": 1
   }
]

```

**PUT /api/movies/<int:movie_id>/review/** - Rota que altera uma crítica já realizada

Não é necessário indicar o id da review a ser alterada, pois cada crítico só poderá ter uma crítica associada ao filme especificado.

```json
// REQUEST
// Header -> Authorization: Token <token-do-critic>
// Todos os campos são obrigatórios
{
  "stars": 2,
  "review": "O Poderoso Chefão 2 podia ter dado muito certo..",
  "spoilers": true
}
```

```json
//RESPONSE
{
  "id": 1,
  "critic": {
    "id": 1,
    "first_name": "Jacques",
    "last_name": "Aumont"
  },
  "stars": 2,
  "review": "O Poderoso Chefão 2 podia ter dado muito certo..",
  "spoilers": true
}
```

Caso seja passado um movie_id inválido ou o crítico tentar fazer uma alteração de uma avaliação de um filme que ele ainda não tenha feito, deverá retornar um erro `HTTP 404 - NOT FOUND`.

```json
// RESPONSE STATUS -> HTTP 404 NOT FOUND
{
  "detail": "Not found."
}
```

## **Tecnologias utilizadas** 📱

- Django
- Django Rest Framework
- PostgreSQL
