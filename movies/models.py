from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=255, unique=True)
    duration = models.CharField(max_length=255)
    premiere = models.DateField()
    classification =  models.IntegerField()
    synopsis = models.TextField()
    genres = models.ManyToManyField('Genre', related_name="movies")


class Genre(models.Model):
    name = models.CharField(max_length=255)
    