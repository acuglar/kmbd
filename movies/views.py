from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView


class MovieView(APIView):
    def post(self, request):
        return Response("MovieView.post")

    def get(self, request):
        return Response("MovieView.get")


class MovieDetailView(APIView):
    def get(self, request, movie_id=''):
        return Response("MovieDetailView.get")

    def delete(self, request, movie_id=''):
        return Response("MovieDetailView.delete")

