from django.urls import path
from accounts.views import SignupView, LoginView


urlpatterns = [
    path('accounts/', SignupView.as_view()),
    path('login/', LoginView.as_view()),
]