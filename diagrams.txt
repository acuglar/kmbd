Table users as U {
  id int [pk, increment] // auto-increment
  username varchar
  password varchar
  first_name varchar
  last_name varchar
  is_staff bool
  is_superuser bool
}

Table movies as M {
  id int [pk]
  title varchar
  duration varchar
  premiere date
  classification integer
  synopsis text
 }
 
Table genres as G {
  id int [pk]
  name varchar
 }

Table movies_genres {
  id int [pk]
  movie_id int [ref: > M.id]
  genre_id int [ref: > G.id]
}

Table reviews as R {
  id int [pk]
  star integer
  review text
  spoiler bool
  user_id integer
  movie_id integer
 }

Ref: U.id < R.user_id
Ref: M.id < R.movie_id
