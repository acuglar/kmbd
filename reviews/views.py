from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView


class ReviewView(APIView):
    def get(self, request):
        return Response("ReviewView.get")


class ReviewDetailView(APIView):
    def post(self, request, movie_id=''):
        return Response("ReviewDetailView.post")

    def put(self, request, movie_id=''):
        return Response("ReviewDetailView.put")